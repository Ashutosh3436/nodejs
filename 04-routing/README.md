## Routing
    
    Rendering multiple pages using `url` module.
    We will target diffrent URL's so that user can visit diffrent pages.
    
    In last example we render a single HTML page.By writing onRequest function and passing it to createServer function in app.js file.
    
## Routing Multiple pages
Here we have 2 html index.html(home page) and login.html(another page).
We will access those pages by using urls for homepage root url(/) and for login (/login).

For this i will create one more file app.js which will handle all requests and a server.js to create our server.

### app.js

Here first we need [url](https://nodejs.org/api/url.html) module and ofcourse our `fs` module
    
```javascript
    var fs = require('fs');
    var url = require('url');
 ```
     
Now lets create handleRequest function to handle our requests and export it.
Here I'm using object system to export our files.
     
 ```javascript
      module.exports = {
        handleRequest: function(request, response) {
            response.writeHead(200, {'Content-Type': 'text/html'});
            
            //fetch the path form url http://locolhost:8000/('/) or /login and store in path variable
            var path = url.parse(request.url).pathname;
            
            //now use switch statement to render file which we read in via url.parse().pathname
            switch (path) {
                case '/' :
                    renderHTML('./index.html', response);
                    break;
                case '/login' :
                    renderHTML('./login.html', response);
                    break;
                 //default if someone request with wrong URL.
                default :
                    response.writeHead(404);
                    response.write('Undefined route');
                    response.end();
            }
        }
      }
 ```
 Now lets write renderHTML method to read our files using fs module
 ```javascript
 function renderHTML(path, response) {
    fs.readFile(path, null, function(error, data) {
        if (error){
            response.writeHead(404);
            response.write('File not found');
        }else {
            response.write(data);
        }
        response.end();
    });
 }
 ```
 
 Now lets import our app module in server.js file and use handleRequest function.
 ```javascript
 var http = require('http');
 var app = require('./app.js');
 
 http.createServer(app.handleRequest).listen('8000');
 ```
 Now start server and access all urls
 
    1.http://localhost:8000/ (to access home page)
    2.http://localhost:8000/login (login page)
    3.http://localhost:8000/any (any wrong url)
  
    
     


     
  