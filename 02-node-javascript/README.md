## Bit About Javascript modules ##
We use the special object module and place a reference of our function into 
```javascript 
module.exports.myFunction = myFunction;
``` 
This lets the CommonJS module system know what we want to expose so that other files can consume it.
Then when someone wants to use that module, they can require it in their file, like so:
```javascript
var module1 = require('./module1);
```
This lets you to export your functions, objects, variables etc from file to another.
In module 1 we are exporting one by one module but its a bit hectic to write export statement for all the
functions and variables. So use `module2.js` method to export.
