## Rendering HTML File as Response 

Earlier we were rendering simple plain text as response. Now lets render HTML file as response.

## There are 2 ways to do that : ##

## First Way Using fs module ##
   First way is to fetching it from our file system for this we will use 
   a node module `fs` known as filesystem.
   
   Using this module we can perform all kind of task with our file system and one of task is to read a file.
   [Read more](https://nodejs.org/api/fs.html) about fs module.
   
### Explanation ###
  To use fs module import it in `app.js`
  ```javascript
    var fs = require('fs');
  ```
  Lets use fs object's method `readFile(path, options, callback)`

  readFile() method takes 3 input arguments :-

  1.path

    First argument is path of your file as string.
   
  2. options
  
    Second used to pass objects or strings.
    
  3.callback 
  
        This callback will be a function which should be executed when nodejs
        finished reading in file.
        
        In this function we are passing 2 arguments one is `error` and other one is
        `data` these 2 arguments will be automatically passed into arguments.
        
### Code ###
```javascript
   fs.readFile('./index.html', null, function(error, data) {
       //if we have any error
       if (error) {
           response.writeHead(404);
           response.write('Not found');
       } 
       //else render data of html file
       else {
           response.write(data);
       }
       //call response.end function inside this callback if write outside there's it get executed before reading file
       response.end();
   });
```
        
    
   

